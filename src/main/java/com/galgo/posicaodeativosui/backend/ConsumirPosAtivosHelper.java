package com.galgo.posicaodeativosui.backend;

import br.com.stianbid.common.MessageExceptionComplexType;
import com.galgo.posicaodeativos.PosicaoDeAtivos;
import com.galgo.posicaodeativosui.AppConfig;
import com.galgo.utils.ApplicationException;
import com.google.common.collect.Lists;
import com.sistemagalgo.schemaposicaoativos.MarkersComplexType;
import com.sistemagalgo.schemaposicaoativos.MessagePosicaoAtivosComplexType;
import com.sistemagalgo.serviceposicaoativos.ConsumirFaultMsg;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import jodd.bean.BeanUtil;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by valdemar.arantes on 03/03/2016.
 */
public class ConsumirPosAtivosHelper {

    private static final Logger log = LoggerFactory.getLogger(ConsumirPosAtivosHelper.class);
    private PosicaoDeAtivos posAtivos;
    private List<MessagePosicaoAtivosComplexType> msgs = Lists.newArrayList();
    private MarkersComplexType lastMarkers = null;


    public ConsumirPosAtivosHelper(PosicaoDeAtivos posDeAtivos) {
        this.posAtivos = posDeAtivos;
    }

    /**
     * Consome as posições de ativos realizaando as paginações. Retorna uma única mensagem com
     * todas as páginas unificadas.
     *
     * @return
     */
    public MessagePosicaoAtivosComplexType consumirTodasAsPaginas() {
            try {
                boolean isLastPage = false;
                int counter = 1;
                do {

                    log.debug("Invocando consumir. counter = {}", counter);
                    MessagePosicaoAtivosComplexType resp = posAtivos.consumir();
                    if (resp == null) {
                        log.debug("return null");
                        return null;
                    }

                    msgs.add(resp);

                    //isLastPage = resp.getGalgoAssBalStmt().getGalgoHdr().isLastPg();
                    isLastPage = (boolean) BeanUtil.getPropertySilently(resp, "galgoAssBalStmt.galgoHdr.lastPg");
                    log.debug("isLastPage = {}", isLastPage);

                    lastMarkers = resp.getGalgoAssBalStmt().getGalgoHdr().getMarkers();
                    if (lastMarkers != null) {
                        posAtivos.setMarkers(lastMarkers.getAssMarker(), lastMarkers.getRptMarker());
                    }

                } while (!isLastPage);
            } catch (ConsumirFaultMsg e) {
                log.error("\n\n{}\n\n", posAtivos.getRequestResponseString());
                final MessageExceptionComplexType consumirFault = e.getFaultInfo().getConsumirFault();
                final String idException = consumirFault.getIdException();
                if (idException.equalsIgnoreCase("PA.0137")) {
                    return joinMsgs();
                }
                if (idException.equalsIgnoreCase("PA.0237")) {
                    throw new ApplicationException(idException + ": " + AppConfig.getInstance().getProperty("err.PA.0237"));
                }
                throw new ApplicationException(idException + ": " + consumirFault.getDsException());
            } catch (Exception e) {
                throw new ApplicationException(e);
            }

        return joinMsgs();
    }

    /**
     * Unifica a lista de mensagens em uma única mensagem
     *
     * @return
     */
    private MessagePosicaoAtivosComplexType joinMsgs() {
        if (CollectionUtils.isEmpty(msgs)) {
            return null;
        }

        if (msgs.size() == 1) {
            return msgs.get(0);
        }

        log.info("Realizando o JOIN das {} mensagens", msgs.size());
        final MessagePosicaoAtivosComplexType msg_ret = msgs.get(0);
        int counter = 0;
        for (MessagePosicaoAtivosComplexType msg : msgs) {
            // Não ha join do primeiro elemento com ele mesmo
            if (counter == 0) {
                counter++;
                continue;
            }

            final List<SubAccountIdentification16> subAcctDtls = msg.getGalgoAssBalStmt().getBsnsMsg().get(0)
                .getDocument().getSctiesBalAcctgRpt().getSubAcctDtls();
            final List<SubAccountIdentification16> subAcctDtls_ret = msg_ret.getGalgoAssBalStmt().getBsnsMsg().get(0)
                .getDocument().getSctiesBalAcctgRpt().getSubAcctDtls();
            joinSubAcctDls(subAcctDtls_ret, subAcctDtls);

            counter++;
        }
        log.info("Fim do JOIN");
        return msg_ret;
    }

    /**
     * Realiza o join dos listas quando o sfkpgAcct.id de ambos os subAcctDtls são iguais. Isto
     * é feito para cada subAcctDtl da lista subAcctDtls.
     *
     * @param subAcctDtls_ret
     * @param subAcctDtls
     */
    private void joinSubAcctDls(final List<SubAccountIdentification16> subAcctDtls_ret,
        final List<SubAccountIdentification16> subAcctDtls) {

        for (SubAccountIdentification16 subAcctDtl : subAcctDtls) {
            joinSubAcct_ListWithElement(subAcctDtls_ret, subAcctDtl);
        }
    }

    /**
     * Realiza o join dos ativos quando o sfkpgAcct.id de ambos os subAcctDtls são iguais. Caso contrário,
     * inclui subAcctDtl na lista subAcctDtls_ret
     *
     * @param subAcctDtls_ret
     * @param subAcctDtl
     * @return True se o join foi feito
     */
    private void joinSubAcct_ListWithElement(final List<SubAccountIdentification16> subAcctDtls_ret,
        final SubAccountIdentification16 subAcctDtl) {
        for (SubAccountIdentification16 subAcctDtl_ret : subAcctDtls_ret) {
            final String sfkpId_ret = subAcctDtl_ret.getSfkpgAcct().getId();
            final String sfkpId = subAcctDtl.getSfkpgAcct().getId();
            if (sfkpId_ret.equals(sfkpId)) {
                subAcctDtl_ret.getBalForSubAcct().addAll(subAcctDtl.getBalForSubAcct());
                return;
            }
        }
        subAcctDtls_ret.add(subAcctDtl);
    }
}

