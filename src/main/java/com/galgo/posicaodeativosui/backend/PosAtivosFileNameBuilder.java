package com.galgo.posicaodeativosui.backend;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Created by Valdemar on 24/01/2016.
 */
public class PosAtivosFileNameBuilder {
    public static String build(Integer codSTI, LocalDate dt) {
        return String.format("pos_ativos_%s_%s.xml", codSTI, dt.format(DateTimeFormatter.ISO_LOCAL_DATE));
    }
}
