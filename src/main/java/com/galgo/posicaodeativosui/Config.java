package com.galgo.posicaodeativosui;

import com.galgo.utils.ApplicationException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

/**
 * Carrega as configuracoes do aplicativo
 *
 * @author valdemar.arantes
 */
public class Config {

    public static final String USERNAME_KEY = "username";
    public static final String PASSWORD_KEY = "password";
    public static final String WS_ADDRESS_HOMOLOG_KEY = "ws.address.homolog";
    public static final String WS_ADDRESS_PRODUCAO_KEY = "ws.address.producao";
    public static final String AMBIENTE_KEY = "ambiente";
    public static final String CONNECTION_RECEIVE_TIMEOUT = "connection.receive.timeout";
    public static final String LAST_SAVE_PATH_KEY = "xml.last_save.path";
    public static final String FOLDER_KEY = "folder";
    private static final Logger log = LoggerFactory.getLogger(Config.class);
    private static Config instance;
    private Properties props;
    private String configPath;
    private File configFile;
    private boolean configFileCreatedInThisExecution = false;
    private String appName;

    static {
        try {
            Properties defaultConfig = new Properties();
            defaultConfig.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "config-default.properties"));
            instance = new Config("posicao_de_ativos", defaultConfig);
        } catch (IOException e) {
            log.error(null, e);
            throw new ApplicationException("Não foi possível instanciar a classe Config");
        }
    }

    /**
     * Sobrepõe as propriedades do arquivo novo nas propriedades desta instância, e já
     * salva o arquivo novo com estas propriedades já combinadas (merge).
     *
     * @param newConfigFile
     * @return
     */
    public Config reload(File newConfigFile) {
        if (!newConfigFile.isFile()) {
            throw new ApplicationException("Arquivo " + newConfigFile.getAbsolutePath() + " não foi encontrado...");
        }

        try {
            log.info("Carregando as propriedades do arquivo {}", newConfigFile.getCanonicalPath());
            final FileReader reader = new FileReader(newConfigFile);
            props.load(reader);
            reader.close();
            configFile = newConfigFile;

            // Necessário para salvar as propriedades do arquivo
            // velho uqe não estavam definidas no novo.
            saveProps();
        } catch (IOException e) {
            throw new ApplicationException("Erro ao ler o arquivo", e);
        }

        return instance;
    }

    private Config(String appName, Properties defaultProperties) throws IOException {
        log.debug("Carregando as configuracoes do aplicativo {}", appName);

        this.appName = appName;

        if (defaultProperties == null) {
            log.warn("Nenhuma propriedade default foi passada");
        } else {
            log.debug("Carregando propriedades default: {}", defaultProperties);
        }

        // Define o formato do ToString das classes do tipo ValueObject
        ToStringBuilder.setDefaultStyle(ToStringStyle.MULTI_LINE_STYLE);

        configFile = getConfigFile(defaultProperties);

        if (configFile == null) {
            throw new ApplicationException("Não foi possível criar o arquivo de configuração");
        }

        log.info("Abrindo o arquivo {}", configPath);
        InputStream is = null;

        try {
            is = new FileInputStream(configFile);
            props = new Properties();
            props.load(is);
            is.close();
            log.debug("config: " + props.toString());
        } catch (Exception e) {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ignored) {
                }
            }
            throw new ApplicationException(e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                }
            }
        }
    }

    /**
     * Recupera a instância criada no método newInstance. Caso seja nula, lança uma exceção
     * ApplicationException.
     *
     * @return
     */
    public static Config getInstance() {
        return instance;
    }

    /**
     * Adiciona e salva as propriedades passadas como parâmetro no arquivo de configuração
     *
     * @param newProperties
     */
    public void addProperties(Map<String, ?> newProperties) {
        props.putAll(newProperties);
        saveProps();
    }

    /**
     * Adiciona e salva a propriedade passada como parâmetro no arquivo de configuração
     *
     * @param key
     * @param value
     */
    public Config addProperty(String key, Object value) {
        log.debug("Incluindo propriedade: {}={}", key, value);
        props.put(key, value);
        saveProps();
        return this;
    }

    /**
     *
     * @param key
     * @param defaultValue
     * @return true quando o valor definido para a chave for "true" (ignore case)
     */
    public boolean getBoolean(String key, boolean defaultValue) {
        Objects.requireNonNull(key);
        if (!props.containsKey(key)) {
            return defaultValue;
        }
        return "true".equalsIgnoreCase(props.getProperty(key));
    }

    public String getCertAlias() {
        return props.getProperty("org.apache.ws.security.crypto.merlin.keystore.alias");
    }

    public String getCertPassword() {
        return props.getProperty("org.apache.ws.security.crypto.merlin.keystore.password");
    }

    public File getConfigFile() {
        return configFile;
    }

    public LocalDate getDate(String key, LocalDate defaultValue) {
        if (props.containsKey(key)) {
            String dateString = getString(key, null);
            return LocalDate.parse(dateString, DateTimeFormatter.ISO_LOCAL_DATE);
        } else if (defaultValue != null) {
            addProperty(key, defaultValue.format(DateTimeFormatter.ISO_LOCAL_DATE));
            return defaultValue;
        } else {
            return null;
        }
    }

    public LocalDateTime getDateTime(String key, LocalDateTime defaultValue) {
        if (props.containsKey(key)) {
            String dateTimeString = getString(key, null);
            return LocalDateTime.parse(dateTimeString, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        } else if (defaultValue != null) {
            addProperty(key, defaultValue.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
            return defaultValue;
        } else {
            return null;
        }
    }

    /**
     * Retorna o inteiro configurado para a chave passada com o argumento key. Se chave não estiver
     * definida ou o valor não for um inteiro válido, retorna o argumento defaultValue.
     *
     * @param key Chave
     * @return
     */
    public Integer getInteger(String key, Integer defaultValue) {
        if (!props.containsKey(key)) {
            log.warn("Arquivo de configuração não possui chave={}. Retornado valor default={}", key, defaultValue);
            if (defaultValue != null) {
                addProperty(key, defaultValue);
            }
            return defaultValue;
        }

        String val = props.getProperty(key);
        if (StringUtils.isBlank(val)) {
            log.warn("Chave={} configurada com brancos. Retornado valor default={}", key, defaultValue);
            if (defaultValue != null) {
                addProperty(key, defaultValue);
            }
            return defaultValue;
        }
        if (!StringUtils.isNumeric(val)) {
            log.warn("Chave={} configurada com valor={}, que não é um número válido.", key, val);
            throw new ApplicationException("Arquivo de configuração com erro na chave " + key + ": o valor " + val +
                " não é um número válido");
        }
        return Integer.valueOf(val);
    }

    /**
     * Recupera a senha do usuário do arquivo de configuração
     *
     * @return
     */
    public String getPassword() {
        if (!props.containsKey(PASSWORD_KEY)) {
            return "";
        }
        return props.getProperty(PASSWORD_KEY);
    }

    /**
     * Salva a senha do usuário no arquivo de configuração
     *
     * @param pwd
     * @return
     */
    public Config setPassword(String pwd) {
        return addProperty(PASSWORD_KEY, pwd);
    }

    public Properties getProperties() {
        return props;
    }

    public String getProperty(String propName) {
        return props.getProperty(propName);
    }

    /**
     * Retorna o valor da chave key. Se a chave não for encontrada, retorna o valor
     * defaultValue, salvando esse valor em arquivo se não for nulo.
     *
     * @param key
     * @param defaultValue
     * @return
     */
    public String getString(String key, String defaultValue) {
        if (props.containsKey(key)) {
            return props.getProperty(key);
        } else if (defaultValue != null) {
            addProperty(key, defaultValue);
            return defaultValue;
        } else {
            return null;
        }
    }

    public LocalTime getTime(String key, LocalTime defaultValue) {
        if (props.containsKey(key)) {
            String timeString = getString(key, null);
            return LocalTime.parse(timeString, DateTimeFormatter.ISO_LOCAL_TIME);
        } else if (defaultValue != null) {
            addProperty(key, defaultValue.format(DateTimeFormatter.ISO_LOCAL_TIME));
            return defaultValue;
        } else {
            return null;
        }
    }

    /**
     * Recupera o login do usuário do arquivo de configuração
     *
     * @return
     */
    public String getUsername() {
        if (!props.containsKey(USERNAME_KEY)) {
            return "";
        }
        return props.getProperty(USERNAME_KEY);
    }

    /**
     * Salva o login do usuário no arquivo de configuração
     *
     * @param login
     */
    public Config setUsername(String login) {
        return addProperty(USERNAME_KEY, login);
    }

    public String getWSAddress() {
        if ("producao".equalsIgnoreCase(props.getProperty(AMBIENTE_KEY))) {
            return props.getProperty(WS_ADDRESS_PRODUCAO_KEY);
        } else {
            return props.getProperty(WS_ADDRESS_HOMOLOG_KEY);
        }
    }

    public boolean isConfigFileCreatedInThisExecution() {
        return configFileCreatedInThisExecution;
    }

    /**
     * Verifica se chave "ambiente" é diferente de "producao"
     *
     * @return
     */
    public boolean isHomologacao() {
        return !"producao".equalsIgnoreCase(props.getProperty(AMBIENTE_KEY));
    }

    /**
     * Caso não seja possível, será utilizada a pasta do aplicativo
     *
     * @return
     */
    private File createInAppFolder() {
        // Caminho do arquivo na pasta do aplicativo
        configPath = SystemUtils.USER_DIR + SystemUtils.FILE_SEPARATOR + "." + appName + SystemUtils.FILE_SEPARATOR +
            "config.properties";
        log.info("configPath={}", configPath);

        File _configFile = new File(configPath);
        if (_configFile.isFile()) {
            log.debug("Arquivo de configuração encontrado.");
            return _configFile;
        }

        log.warn("Arquivo {} não encontrado", _configFile);

        String parent = _configFile.getParent();
        log.debug("_configFile.parent={}", parent);
        File parentFolder = new File(parent);
        if (!parentFolder.exists()) {
            log.info("Pasta {} nao encontrada. Criando...", parentFolder);
            if (!parentFolder.mkdirs()) {
                log.warn("Não foi possível criar a pasta " + parentFolder);
                return null;
            }
        }

        configFileCreatedInThisExecution = true;
        return _configFile;
    }

    /**
     * As configuraçõees tentarão ser mantidas na pasta USER_HOME\.<NOME_APP>\config.properties
     *
     * @return
     */
    private File createInUserHomeFolder() throws IOException {
        configPath = SystemUtils.USER_HOME + SystemUtils.FILE_SEPARATOR + "." + appName + SystemUtils.FILE_SEPARATOR
            + "config.properties";
        log.info("configPath={}", configPath);

        File _configFile = new File(configPath);
        if (_configFile.isFile()) {
            log.debug("Arquivo {} já existe. Encerrando este método...", _configFile.getCanonicalPath());
            return _configFile;
        }

        String parent = _configFile.getParent();
        log.debug("_configFile.parent={}", parent);
        File parentFolder = new File(parent);
        if (parentFolder.exists() && !parentFolder.isDirectory()) {
            log.warn(parentFolder + " já existe e não é uma pasta!");
            return null;
        } else if (!parentFolder.exists()) {
            log.info("Pasta {} nao encontrada. Criando...", parentFolder);
            if (!parentFolder.mkdirs()) {
                log.warn("Não foi possível criar a pasta " + parentFolder);
                return null;
            }
        }

        configFileCreatedInThisExecution = true;
        return _configFile;
    }

    /**
     * Verifica se o arquivo config.properties existe na pasta
     * <USER_HOME>\.<APP_NAME>. Se existir, retorna o arquivo, se nao existir, retorna as
     * propriedades default passadas no construtor desta classe atribui ao campo
     *
     * @.
     */
    private File getConfigFile(Properties defaultProperties) throws IOException {
        File _configFile;

        // Tenta carregar as configurações do arquivo configurado em etc/app.properties. Caso não consigo, utiliza
        // o arquivo da psta do usuário
        String configFileProp = MainUI.getInstance().externalConfig.getProperty(ConfigFolderPreferences.EXT_CONFIG_PATH_KEY);
        if (StringUtils.isNotBlank(configFileProp)) {
            log.info("Carregando o arquivo com as configurações: {}", configFileProp);
            _configFile = new File(configFileProp);
            if (!_configFile.isFile()) {
                log.info("Arquivo {} não carregou. Utilizando a pasta do usuário (default)...");
                _configFile = createInUserHomeFolder();
            }
        } else {
            _configFile = createInUserHomeFolder();
        }

        if (_configFile == null) {
            _configFile = createInAppFolder();
        }

        if (_configFile == null) {
            throw new ApplicationException("Não foi possível criar o arquivo de configuração do aplicativo");
        }

        configFile = _configFile;

        if (configFileCreatedInThisExecution) {
            props = (defaultProperties == null ? new Properties() : defaultProperties);
            log.debug("Arquivo {} criado nessa execução.\nSalvando props default {}", configFile, props);
            saveProps();
        }

        return configFile;
    }

    /**
     * Salva as propriedade no arquivo de configuração
     */
    private void saveProps() {
        OutputStream outStream = null;
        try {
            log.debug("Salvando as configurações em {}", configFile);
            outStream = new FileOutputStream(configFile);
            props.store(outStream, "Arquivo de configuracao");
            outStream.close();
        } catch (Exception e) {
            if (outStream != null) {
                try {
                    outStream.close();
                } catch (IOException ex) {
                }
            }
            throw new ApplicationException(e);
        }
    }
}
