package com.galgo.posicaodeativosui;

import com.google.common.collect.Maps;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Created by valdemar.arantes on 04/05/2016.
 */
public class VpnPreferences {

    private static final Logger log = LoggerFactory.getLogger(VpnPreferences.class);
    private AppConfig appConf = AppConfig.getInstance();
    private Config userConf = Config.getInstance();

    @FXML private GridPane bodyPane;

    @FXML private CheckBox chkEnabled;

    @FXML private Label lblLogin;

    @FXML private TextField txtLogin;

    @FXML private Label lblName;

    @FXML private TextField txtName;

    @FXML private Label lblPwd;

    @FXML private PasswordField pwd;

    @FXML private Button btnConfirmar;

    @FXML private Button btnCancelar;

    public VpnPreferences() {
    }

    @FXML
    void initialize() {
        initializeFormValues();
    }

    private void initializeFormValues() {
        initializeTextField(txtLogin, "app.vpn.user");
        initializeTextField(txtName, "app.vpn.name");
        initializeTextField(pwd, "app.vpn.pwd");
        if (StringUtils.isNotBlank(userConf.getProperty("app.vpn.controlled"))) {
            chkEnabled.setSelected("true".equalsIgnoreCase(userConf.getProperty("app.vpn.controlled")));
        }
    }

    private void initializeTextField(TextField txtField, String configKey) {
        if (StringUtils.isNotBlank(userConf.getProperty(configKey))) {
            if ("app.vpn.pwd".equals(configKey)) {
                txtField.setText(CryptoUtils.decrypt(userConf.getProperty(configKey)));
            } else {
                txtField.setText(userConf.getProperty(configKey));
            }
        }
    }

    @FXML
    void handleBtnConfirmarKeyPressed(KeyEvent event) {
        if (KeyCode.ENTER.equals(event.getCode())) {
            log.info("ENTER no botão Confirmar");
            saveFormValues();
            ((Stage)bodyPane.getScene().getWindow()).close();
        }
    }

    @FXML
    private void handleBtnConfirmarAction(ActionEvent event) {
        log.info("Click no botão Confirmar");
        saveFormValues();
        ((Stage)bodyPane.getScene().getWindow()).close();
    }

    @FXML
    private void handleBtnCancelarAction(ActionEvent event) {
        log.info("Botão cancelar clicado. Fechando a popup window...");
        ((Stage)bodyPane.getScene().getWindow()).close();
    }

    private void saveFormValues() {
        log.info("Salvando valores do formulário");

        Map<String, String> props = Maps.newHashMap();
        props.put("app.vpn.user", txtLogin.getText());
        props.put("app.vpn.name", txtName.getText());
        props.put("app.vpn.pwd", CryptoUtils.encrypt(pwd.getText()));
        props.put("app.vpn.controlled", String.valueOf(chkEnabled.isSelected()));

        userConf.addProperties(props);
    }
}
