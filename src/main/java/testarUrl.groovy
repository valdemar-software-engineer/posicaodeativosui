import java.time.LocalTime
/**
 * Created by valdemar.arantes on 19/05/2016.
 */
def url = "http://www.sistemagalgo.com.br".toURL()
while (true) {
    f(url)
    // Intervalo de 10 seg entre cada teste
    Thread.sleep(10_000)
}

/*
url.eachLine {
    println it
}
*/

def f(URL url) {
    println "${LocalTime.now()} Abrindo a conexão com $url ..."
    def conn = (HttpURLConnection) url.openConnection()
    if (HttpURLConnection.HTTP_OK == conn.getResponseCode()) {
        println "${LocalTime.now()} FIM!!!"
        System.exit(0)
    }
    println "Code ${conn.getResponseCode()}"
    conn.disconnect()
}