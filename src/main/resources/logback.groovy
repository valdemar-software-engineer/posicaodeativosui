//
// Built on Fri May 06 18:17:18 UTC 2016 by logback-translator
// For more information on configuration files in Groovy
// please see http://logback.qos.ch/manual/groovy.html

// For assistance related to this tool or configuration files
// in general, please contact the logback user mailing list at
//    http://qos.ch/mailman/listinfo/logback-user

// For professional support please see
//   http://www.qos.ch/shop/products/professionalSupport

import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.core.ConsoleAppender
import ch.qos.logback.core.FileAppender
import java.nio.charset.Charset

import static ch.qos.logback.classic.Level.ALL
import static ch.qos.logback.classic.Level.INFO

def bySecond = timestamp("yyyyMMdd'T'HHmmss")
appender("STDOUT", ConsoleAppender) {
    def idea = System.getProperty('idea.launcher.bin.path')
    def rebel = System.getProperty('rebel.env.ide')
    def javaCommand = System.getProperty("sun.java.command");
/*
    println "idea.launcher.bin.path=$idea;\nrebel.env.ide=$rebel"
    println "System.console()=${(boolean) System.console()}"
    println "${(!idea && !rebel && !javaCommand.contains("intellij") && System.console())}"
*/

    encoder(PatternLayoutEncoder) {
        pattern = "%d{HH:mm:ss.SSS} [%thread] %-5level %logger{5} [%M] - %m .\\(%class{0}.java:%line\\)%n"
        if (!idea && !rebel && !javaCommand.contains("intellij") && System.console()) {
            println 'Utilizando o charset cp850 no log'
            charset = Charset.forName("cp850")
        } else {
            charset = Charset.forName("utf-8")
            println "Utilizando o charset default: $charset"
        }
    }
}
appender("FILE", FileAppender) {
    file = "./log/log-${bySecond}.txt"
    append = true
    encoder(PatternLayoutEncoder) {
        pattern = "%d{HH:mm:ss.SSS} [%thread] %-5level %logger{5} [%M] - %m .\\(%class{0}.java:%line\\)%n"
    }
}
logger("org.apache", INFO, [], false)
logger("com.galgo.cxfutils.sec.PasswordCallbackHandler", INFO, [], false)
root(ALL, ["STDOUT", "FILE"])